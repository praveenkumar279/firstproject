const inventory = require('./cars.cjs')
let BMWAndAudi = [];

function problem6(inventory){
    if(!inventory){
        return [];
    }
    
    if(!Array.isArray(inventory)){
        return [];
    }

    for(let index=0; index<inventory.length; index++){
        if(inventory[index].car_make == "BMW" || inventory[index].car_make == "Audi"){
            BMWAndAudi.push(inventory[index]);
        }
    }
    return BMWAndAudi;
}

module.exports = problem6;