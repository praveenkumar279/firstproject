const inventory = require('./cars.cjs')
let carYears = [];
let olderCars = [];
function problem5(inventory){
    if(!inventory){
        return [];
    }
    
    if(!Array.isArray(inventory)){
        return [];
    }

    for(let index=0; index<inventory.length;index++){
        carYears.push(inventory[index].car_year);
    }

    for(let index=0; index<carYears.length; index++){
        if(carYears[index] < 2000){
            olderCars.push(carYears[index]);
        }
    }
    return olderCars;
}

module.exports = problem5;