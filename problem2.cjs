
const inventory = require('./cars.cjs')

function problem2(inventory){
    if(!inventory){
        return [];
    }
    if(inventory.length == 0){
        return [];
    }
    if(!Array.isArray(inventory)){
        return [];
    }
    
    return inventory[inventory.length-1];
    
}

module.exports = problem2;