const problem1 = require('../problem1.cjs')
const inventory = require('../cars.cjs');

const result1 = problem1(inventory,33);

if(result1.length==0){
    console.log(result1);
}
else{
    console.log("Car "+result1.id+" is a "+result1.car_year+" "+result1.car_make+" "+result1.car_model);
}