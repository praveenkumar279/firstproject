const problem2 = require('../problem2.cjs')
const inventory = require('../cars.cjs');

const result2 = problem2(inventory);


if(result2.length==0){
    console.log(result2);
}
else{
    console.log("Last car is a "+inventory[inventory.length-1].car_make+" "+inventory[inventory.length-1].car_model);
}