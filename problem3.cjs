const inventory = require('./cars.cjs')

function problem3(inventory){
    if(!inventory){
        return [];
    }
    if(inventory.length == 0){
        return [];
    }
    if(!Array.isArray(inventory)){
        return [];
    }

    inventory.sort((a,b)=>{
        if(a.car_model.toUpperCase() < b.car_model.toUpperCase()){
            return -1;
        }
        else if(a.car_model.toUpperCase() > b.car_model.toUpperCase()){
            return 1;
        }
        else{
            return 0;
        }
    })
    return inventory;
}

module.exports = problem3;