
const inventory = require('./cars.cjs');

function problem1(inventory,id){
    if(!inventory){
        return [];
    }
    if(!Array.isArray(inventory)){
        return [];
    }
    for(let index=0;index<inventory.length;index++){
        if(inventory[index].id===id){
            return inventory[index];
        }
    }
    return [];
}
module.exports = problem1;