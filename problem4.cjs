const inventory = require('./cars.cjs')
const carYears = [];
function problem4(inventory){
    if(!inventory){
        return [];
    }
    if(!Array.isArray(inventory)){
        return [];
    }

    for(let index=0; index<inventory.length;index++){
        carYears.push(inventory[index].car_year);
    }
    return carYears;
}

module.exports = problem4;